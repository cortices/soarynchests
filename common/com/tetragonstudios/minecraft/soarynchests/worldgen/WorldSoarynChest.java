package com.tetragonstudios.minecraft.soarynchests.worldgen;

import java.util.Random;
import java.util.logging.Level;

import cpw.mods.fml.common.FMLLog;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.gen.feature.WorldGenerator;

public class WorldSoarynChest extends WorldGenerator{
	private static int chestId = Block.chest.blockID;
	private int metadata;

	public WorldSoarynChest()
	{
	}

	public boolean generate(World world, Random random, int x, int y, int z)
	{
		world.setBlock(x, y, z, chestId);
		//world.setBlock(x+1, y+1, z, Block.torchWood.blockID);

		TileEntityChest chest = (TileEntityChest)world.getBlockTileEntity(x, y, z);
		if (chest != null) {
			for (int slot = 0; slot < chest.getSizeInventory(); ++slot) {
				if (random.nextInt(2) == 0) {
					;
				}
				else {
					if (random.nextInt(2) == 0) {
						chest.setInventorySlotContents(slot, new ItemStack(Block.dirt, random.nextInt(48)+1));
					}
					else {
						chest.setInventorySlotContents(slot, new ItemStack(Block.cobblestone, random.nextInt(48)+1));
					}
				}

			}
		}
		else {
			FMLLog.log(Level.WARNING, "Chest TileEntity not present");
		}

		return true;
	}
}