package com.tetragonstudios.minecraft.soarynchests;

import java.io.File;
import java.util.logging.Level;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.Configuration;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Init;
import cpw.mods.fml.common.Mod.PreInit;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

@Mod(modid = SoarynChests.modID, name = "SoarynChests", version = "1.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class SoarynChests
{
	public static final String modID = "SoarynChests";
	public static Configuration config;
	
	public static int freqChests;

	//public static Block tutorialBlock;

	EventManager eventManager = new EventManager();

	@PreInit
	public void preInit(FMLPreInitializationEvent event) {
		config = new Configuration(event.getSuggestedConfigurationFile());

		try {
			config.load();
			freqChests = config.get(Configuration.CATEGORY_GENERAL, "chestFrequency", "16").getInt();
			config.save();

		} catch (Exception e) {
			FMLLog.log(Level.SEVERE, e, modID + " has had a problem loading its configuration");
		}
	}

	@Init
	public void load(FMLInitializationEvent event)
	{
		GameRegistry.registerWorldGenerator(eventManager);
		FMLLog.log(Level.INFO, modID + " has loaded");
	}
}