package com.tetragonstudios.minecraft.soarynchests;

import com.tetragonstudios.minecraft.soarynchests.worldgen.EndSoarynChest;
import com.tetragonstudios.minecraft.soarynchests.worldgen.NetherSoarynChest;
import com.tetragonstudios.minecraft.soarynchests.worldgen.WorldSoarynChest;

import java.util.Random;
import java.util.logging.Level;

import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;
import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.IWorldGenerator;

public class EventManager implements IWorldGenerator
{
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider)
	{
		switch(world.provider.dimensionId)
		{
		case -1: generateNether(world, random, chunkX * 16, chunkZ * 16);
		case 0: generateSurface(world, random, chunkX * 16, chunkZ * 16);
		case 1: generateEnd(world, random, chunkX * 16, chunkZ * 16);
		default: generateSurface(world, random, chunkX * 16, chunkZ * 16);
		}
	}

	private void generateSurface(World world, Random random, int x, int z)
	{
		// freqChests is how often we want chests to spawn, as 1 per X chunks
		if(random.nextInt(SoarynChests.freqChests) == 0) {
			int Xb = x + random.nextInt(16);
			int Zb = z + random.nextInt(16);
			int Yb;

			for (Yb = 128; !(world.getBlockId(Xb, Yb-1, Zb) == Block.grass.blockID) && Yb > 0; Yb--) {
				;
			}

			FMLLog.log(Level.FINEST, "Generating SoarynChest at: 0:%d,%d,%d", Xb, Yb, Zb);

			if(Yb > 1) {
				(new WorldSoarynChest()).generate(world, random, Xb, Yb, Zb);
			}
		}
	}

	private void generateNether(World world, Random random, int x, int z)
	{
		if(random.nextInt(SoarynChests.freqChests) == 0) {
			int Xb = x + random.nextInt(16);
			int Zb = z + random.nextInt(16);
			int Yb;

			for (Yb = random.nextInt(126); !(world.getBlockId(Xb, Yb-1, Zb) == Block.netherrack.blockID && world.isAirBlock(Xb, Yb, Zb) && world.isAirBlock(Xb, Yb+1, Zb)) && Yb > 0; Yb--) {
				;
			}

			FMLLog.log(Level.FINEST, "Generating SoarynChest at: -1:%d,%d,%d", Xb, Yb, Zb);

			if(Yb > 1) {
				(new NetherSoarynChest()).generate(world, random, Xb, Yb, Zb);
			}
		}
	}
	
	private void generateEnd(World world, Random random, int x, int z)
	{
		if(random.nextInt(SoarynChests.freqChests) == 0) {
			int Xb = x + random.nextInt(16);
			int Zb = z + random.nextInt(16);
			int Yb;

			for (Yb = 128; !(world.getBlockId(Xb, Yb-1, Zb) == Block.whiteStone.blockID) && Yb > 0; Yb--) {
				;
			}

			FMLLog.log(Level.FINEST, "Generating SoarynChest at: 1:%d,%d,%d", Xb, Yb, Zb);

			if(Yb > 1) {
				(new EndSoarynChest()).generate(world, random, Xb, Yb, Zb);
			}
		}
	}

}