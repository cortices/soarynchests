#About
Minecraft forge mod that adds random chests with bits of dirt and cobblestone all over the world, making Soaryn completely obsolete :D

Also makes chests with Netherrack and Soul Sand in the Nether, and End Stone and Obsidian in the End, just like Soaryn would!

#Download
###Version 1.0 -- *1.5.2*
<https://www.dropbox.com/s/nzweuuqymqzikwk/SoarynChests-1.0.zip>
